import React from 'react';
import {View, StyleSheet, Dimensions, Platform} from 'react-native';

export function getContainerWidth() {
  const realDeviceWidth = Dimensions.get('window').width;
  if (Platform.OS !== 'web') {
    return realDeviceWidth;
  }
  return realDeviceWidth > 400 ? 400 : realDeviceWidth;
}

export function DesktopLayout({children}: {children: React.ReactNode}) {
  return (
    <View style={styles.container}>
      <View style={styles.mobileViewportLimiter}>{children}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  mobileViewportLimiter: {
    width: getContainerWidth(),
    height: '100%',
  },
});
