import {Dimensions, ImageRequireSource} from 'react-native';
import {cloneDeep} from 'lodash';

const {height} = Dimensions.get('window');
import {getContainerWidth} from './DesktopLayout';
const width = getContainerWidth();
export const responsiveHeight = (h: number): number => height * (h / 100);
export const WIDTH = (w: number): number => width * (w / 414);
export const HEIGHT = (h: number): number => height * (h / 896);
export const getWidth = (): number => width;
export const getHeight = (): number => height;
export const getLineHeight = (f: number): number => f;
export const getFont = (f: number): number => f - 1;
export function capitalizeFirstLetter(string: string): string {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

//
export function deepCloneObject<T>(obj: T): T {
  return cloneDeep(obj);
}
