import * as React from 'react';
import {Text, Image} from 'react-native';
import {Dimensions} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import Home from '../../Component/Home/index';
import Review from '../../Component/Review/index';
import Marketplace from '../../Component/Marketplace/index';
import Map from '../../Component/Map/index';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

//Stack Navigation
import ScanQRcodeScreen from '../../Component/ScanQRcode/index';
import DetailProductScreen from '../../Component/DetailProduct/index';
import ScanQRCodeByImage from '../../Component/ScanQRCodeByImage';
import DetailNew from '../../Component/DetailNew';

const StackNavigation = createStackNavigator();
const Tab = createBottomTabNavigator();

function HomeStack() {
  return (
    <StackNavigation.Navigator headerMode="none">
      <StackNavigation.Screen name="Home" component={Home} />
    </StackNavigation.Navigator>
  );
}

function ReviewStack() {
  return (
    <StackNavigation.Navigator>
      <StackNavigation.Screen
        name="Review"
        component={Review}
        options={{headerShown: false}}
      />
    </StackNavigation.Navigator>
  );
}

function MarketplaceStack() {
  return (
    <StackNavigation.Navigator>
      <StackNavigation.Screen
        name="Marketplace"
        component={Marketplace}
        options={{headerShown: false}}
      />
    </StackNavigation.Navigator>
  );
}

function MapStack() {
  return (
    <StackNavigation.Navigator headerMode="none">
      <StackNavigation.Screen
        name="Map"
        component={Map}
        options={{headerShown: false}}
      />
    </StackNavigation.Navigator>
  );
}

function BottomTab() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showIcon: true,
        showLabel: true,
        style: {
          backgroundColor: '#fff',
          height: Dimensions.get('window').height / 12,
        },
        tabStyle: {
          paddingVertical: 10,
        },
        labelStyle: {
          fontWeight: 'bold',
        },
        keyboardHidesTabBar: true,
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          switch (route.name) {
            case 'HomeTab':
              return focused ? (
                <FontAwesome name="home" color="#1db853" size={26} />
              ) : (
                <FontAwesome name="home" color="#808080" size={23} />
              );
            case 'ReviewTab':
              return focused ? (
                <Ionicons name="reader-outline" color="#1db853" size={25} />
              ) : (
                <Ionicons name="reader-outline" color="#808080" size={22} />
              );
            case 'MarketplaceTab':
              return focused ? (
                <FontAwesome5 name="store" color="#1db853" size={22} />
              ) : (
                <FontAwesome5 name="store" color="#808080" size={19} />
              );
            case 'MapTab':
              return focused ? (
                <FontAwesome name="map-o" color="#1db853" size={23} />
              ) : (
                <FontAwesome name="map-o" color="#808080" size={20} />
              );
            default:
              break;
          }
        },
      })}
      tabBarOptions={{
        activeTintColor: '#1db853',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen
        name="HomeTab"
        component={HomeStack}
        options={{tabBarLabel: 'Home'}}
      />
      <Tab.Screen
        name="ReviewTab"
        component={ReviewStack}
        options={{tabBarLabel: 'Farm'}}
      />
      <Tab.Screen
        name="MarketplaceTab"
        component={MarketplaceStack}
        options={{
          tabBarLabel: 'Partner',
        }}
      />
      <Tab.Screen
        name="MapTab"
        component={MapStack}
        options={{
          tabBarLabel: 'Contact',
        }}
      />
    </Tab.Navigator>
  );
}
///////////////////////////////
///////KHAI BÁO ĐIỀU HƯỚNG Ở ĐÂY NẾU >>>KHÔNG MUỐN HIỂN THỊ TAB BAR<<<<
///////////////////////////////
function App() {
  return (
    <StackNavigation.Navigator screenOptions={{headerShown: false}}>
      <StackNavigation.Screen name="BottomTab" component={BottomTab} />
      <StackNavigation.Screen
        name="ScanQRcodeScreen"
        component={ScanQRcodeScreen}
      />
      <StackNavigation.Screen
        name="ScanQRCodeByImage"
        component={ScanQRCodeByImage}
      />
      <StackNavigation.Screen name="DetailNew" component={DetailNew} />
      <StackNavigation.Screen
        name="DetailProductScreen"
        component={DetailProductScreen}
      />
    </StackNavigation.Navigator>
  );
}

export default App;
