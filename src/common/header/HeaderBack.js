import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {getWidth, HEIGHT, WIDTH} from '../../configs/functions';
import {useNavigation} from '@react-navigation/native';
const HeaderBack = ({onButton}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={navigation.goBack}>
        <AntDesign name="arrowleft" color="#fff" size={HEIGHT(40)} />
      </TouchableOpacity>
    </View>
  );
};
export default HeaderBack;
const styles = StyleSheet.create({
  container: {
    height: HEIGHT(80),
    width: getWidth(),
    backgroundColor: '#009387',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: WIDTH(12),
  },
});
