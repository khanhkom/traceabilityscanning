import ApiService, {API_LIST, rootAPI} from '.';

class ApiScanQR extends ApiService {
  async APIScanQRcode(body) {
    res = await rootAPI.apigetData(API_LIST.LotProductIdByName, body);
    return res;
  }
  async APIgetTracabilityInfo(body) {
    res = await rootAPI.apigetData(API_LIST.TracabilityInfo, body);
    return res;
  }
  async APIgetTracabilityTimeline(body) {
    res = await rootAPI.apigetData(API_LIST.TracabilityTimeline, body);
    return res;
  }
  async APIgetListPost(body) {
    let res = await rootAPI.getData(API_LIST.GetListPost, body);
    return res;
  }
}

export default new ApiScanQR();
