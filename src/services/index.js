// import {Alert, Platform, PlatformIOS} from 'react-native';

import * as APICONST from './constant';

export const API_LIST = {
  LotProductIdByName: 'getLotProductIdByName',
  TracabilityInfo: 'getTracabilityInfo',
  TracabilityTimeline: 'getTracabilityTimeline',
  GetListPost:
    'http://lina.network/wp-json/wp/v2/posts?items=id,title,featured_media&_embed',
};
// const ERROR_MESSAGES = {
//   405: 'Không có quyền truy cập vào chức năng này!',
//   404: 'Không tồn tại URL!',
//   401: 'Gia hạn token thất bại!',
//   406: 'Định dạng token không đúng!',
//   410: 'Token bị vô hiệu do đã có phiên login khác!',
//   400: 'Không tìm thấy nội dung yêu cầu!',
//   500: 'Lỗi hệ thống (Internal server error).',
// };

export default class ApiService {
  static serverURL;
  serverURL = APICONST.SERVER.DEV;
  async apigetData(apiName, body = null) {
    console.log(body);
    console.log(apiName);
    let url = this.serverURL + APICONST.SERVER.API_TYPE;
    let urlapi = url + apiName;
    let bodyStr = '';
    if (body) {
      Object.keys(body).map((item) => {
        bodyStr += item + ':' + "'" + body[item] + "'" + ',';
      });
    }
    bodyStr = bodyStr.slice(0, -1);
    console.log('bodyStr');
    console.log(urlapi);
    try {
      let response = await fetch(urlapi, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      });
      // console.log(response);

      if (response.status === 200) {
        let responseJson = await response.json();
        return responseJson;
      }
      //   else if (response.status === 401) {
      //     let responseJson = await response.json();
      //     return responseJson;
      //   }
      else {
        console.log('FAILED');
        return null;
      }
    } catch (error) {
      console.log('------API ERROR-------');
      console.log(error);
      return null;
    }
  }
  async getData(apiName, body = null) {
    let url = '';
    let urlapi = url + apiName;
    console.log(urlapi);
    try {
      let response = await fetch(urlapi, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: body,
      });
      if (response.status === 200) {
        let responseJson = await response.json();
        return responseJson;
      } else {
        console.log('FAILED');
        return null;
      }
    } catch (error) {
      console.log('------API ERROR-------');
      console.log(error);
      return null;
    }
  }
}

export const rootAPI = new ApiService();
