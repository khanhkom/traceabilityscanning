import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {getHeight, getWidth, HEIGHT} from '../../configs/functions';
import HeaderBack from '../../common/header/HeaderBack';
const {width, height} = Dimensions.get('window');
const Marketplace = ({route}) => {
  const [link, setLink] = useState('https://farmtrust.vn/en/homepage/');
  const [loading, setLoading] = useState(true);
  return (
    <View style={styles.container}>
      {/* <HeaderBack /> */}
      {loading && (
        <ActivityIndicator
          size="large"
          color="#1db853"
          style={{
            top: height / 2,
            position: 'absolute',
            zIndex: 1,
            alignSelf: 'center',
          }}
        />
      )}
      <View style={{flex: 1}}>
        <WebView
          source={{
            uri: link ?? 'https://farmtrust.vn/en/homepage/',
            // uri: 'https://reactnative.dev/',
          }}
          onError={(err) => {
            console.log('err_err', err);
          }}
          style={styles.webview}
          // onLoadStart={() => {
          //   setLoading(true);
          // }}
          onLoad={() => {
            setLoading(false);
          }}
        />
      </View>
    </View>
  );
};
export default Marketplace;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: getHeight(),
    backgroundColor: '#fff',
  },
  webview: {
    height: HEIGHT(500),
    width: getWidth(),
  },
});
