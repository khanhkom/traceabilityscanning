import React, {Component} from 'react';
import {View, useWindowDimensions} from 'react-native';
import HTML from 'react-native-render-html';
import {WIDTH} from '../../configs/functions';

export default function TextHTML({data, bold}) {
  const contentWidth = WIDTH(200);
  return (
    <View>
      <HTML
        source={{html: data}}
        contentWidth={contentWidth}
        containerStyle={{width: WIDTH(260)}}
        baseFontStyle={{fontWeight: bold ? 'bold' : 'normal'}}
      />
    </View>
  );
}
