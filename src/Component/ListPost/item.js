import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {getFont, getWidth, HEIGHT, WIDTH} from '../../configs/functions';
import ApiScanQR from '../../services/ApiScanQR';
import TextHTML from './TextHTML';
const Item = ({item, onPress}) => {
  console.log(item?._embedded?.['wp:featuredmedia']?.[0]?.link)
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Image
        resizeMode="stretch"
        style={styles.image}
        source={{
          uri:
            item?._embedded?.['wp:featuredmedia']?.[0]?.link ??
            'https://www.nhatbanaz.com/wp-content/uploads/3322_pixta_30838512_L-310x205.jpg',
        }}
      />
      <View style={styles.wrapperDes}>
        <TextHTML data={item?.title?.rendered ?? ''} bold />
        <TextHTML data={item?.excerpt?.rendered ?? ''} />
        {/* <Text style={styles.title}>{item?.title?.rendered ?? ''}</Text>
        <Text style={styles.expert}>{item?.excerpt?.rendered ?? ''}</Text> */}
      </View>
    </TouchableOpacity>
  );
};
export default Item;
const styles = StyleSheet.create({
  container: {
    width: getWidth(),
    flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'center',
    paddingVertical: HEIGHT(10),
    borderTopColor: '#ddd',
    borderTopWidth: 1,
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
    paddingHorizontal: WIDTH(14),
    marginVertical: HEIGHT(10),
  },
  image: {
    height: WIDTH(60),
    width: WIDTH(80),
    borderRadius: 10,
    marginTop: HEIGHT(30),
  },
  wrapperDes: {
    marginLeft: WIDTH(15),
  },
  title: {
    color: '#000',
    fontSize: getFont(16),
    fontWeight: 'bold',
    width: WIDTH(260),
  },
  expert: {
    color: '#000',
    fontSize: getFont(14),
    fontWeight: 'normal',
    width: WIDTH(260),
  },
});
