import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  Image,
} from 'react-native';
import {getFont, getWidth, HEIGHT} from '../../configs/functions';
import ApiScanQR from '../../services/ApiScanQR';
import {ScrollView} from 'react-native-gesture-handler';
import Item from './item';
const ImageAboutFarmtrust = require('../../../assets/home/cover-2.jpg');
const ImageBusinessConsulting = require('../../../assets/home/cover-1-1.jpg');
const ImageBlockChain = require('../../../assets/home/Blockchain.jpg');
const ImageAboutUs = require('../../../assets/home/tu-van-doanh-nghiep-cropped.jpg');
const linkData = [
  {
    id: 0,
    link: 'https://linasupplychain.com/en/farmtrust-en/',
    name: 'About Farmtrust',
    image: ImageAboutFarmtrust,
  },
  {
    id: 1,
    link: 'https://linasupplychain.com/en/business-consulting/',
    name: 'Business Consulting',
    image: ImageBusinessConsulting,
  },
  {
    id: 2,
    link: 'https://linasupplychain.com/en/blockchain-en/',
    name: 'Blockchain',
    image: ImageBlockChain,
  },
  {
    id: 3,
    link: 'https://linasupplychain.com/en/management-team/',
    name: 'About Us',
    image: ImageAboutUs,
  },
];
const {width, height} = Dimensions.get('window');
const ListPost = ({navigation}) => {
  const [listPost, setListPost] = useState([]);
  const [loading, setLoading] = useState(false);
  // navigation.navigate('ScanQRcodeScreen');
  useEffect(() => {
    async function getListPost() {
      try {
        setLoading(true);
        let resPost = await ApiScanQR.APIgetListPost();
        // console.log('resPost_resPost', resPost);
        setLoading(false);
        setListPost(resPost);
      } catch (error) {
        setLoading(false);
        console.log('error_error', error);
      }
    }
    getListPost();
  }, []);
  const ScrollViewTo = (xPos) => {
    Myscroll.scrollTo({
      x: xPos,
      y: 0,
      animated: true,
    });
  };
  return (
    <View style={styles.container}>
      {/* <Text style={styles.label}>News</Text> */}
      <ScrollView
        style={{
          marginBottom: 6,
          paddingVertical: 6,
          height: 150,
        }}
        horizontal={true}
        ref={(ref) => (Myscroll = ref)}
        // showsVerticalScrollIndicator={false}
        // showsHorizontalScrollIndicator={false}
      >
        {linkData.map((item, index) => {
          return (
            <TouchableOpacity
              key={index}
              onPress={() => {
                navigation.navigate('DetailNew', {item: item});
                ScrollViewTo((index * width) / 2);
              }}
              style={styles.bottomcategory}>
              <ImageBackground source={item.image} style={styles.imagecategory}>
                <View style={styles.fadecategory}>
                  <Text style={styles.txtcategory}>{item.name}</Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
      <ScrollView>
        {loading ? (
          <View>
            <ActivityIndicator size="large" color="#1db853" />
          </View>
        ) : (
          <FlatList
            data={listPost}
            // contentContainerStyle={styles.flatlistContainer}
            style={styles.flatlist}
            renderItem={({item, index}) => {
              return (
                <Item
                  item={item}
                  onPress={() => navigation.navigate('DetailNew', {item: item})}
                />
              );
            }}
            keyExtractor={(item, index) => String(index)}
          />
        )}
      </ScrollView>
    </View>
  );
};
export default ListPost;
const styles = StyleSheet.create({
  container: {
    // height: 60,
    // width: 120,
    backgroundColor: '#fff',
    alignSelf: 'center',
    marginTop: 10,
    // justifyContent: 'center',
    alignItems: 'center',
    width: getWidth(),
    // flex: 1,
  },
  label: {
    fontWeight: 'bold',
    marginVertical: HEIGHT(20),
    fontSize: getFont(20),
  },
  flatlist: {
    // flex: 1,
    marginBottom: 200,
  },
  bottomcategory: {
    paddingHorizontal: 3,
    paddingBottom: 18,
    borderRadius: 3,
    marginHorizontal: 3,
    borderRadius: 6,
  },
  imagecategory: {
    width: 160,
    height: 100,
    overflow: 'hidden',
    borderRadius: 6,
  },
  fadecategory: {
    height: 100,
    borderRadius: 6,
    backgroundColor: 'rgba(54,54,54,0.4)',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingVertical: 12,
  },
  txtcategory: {
    fontSize: 16,
    color: 'white',
    fontWeight: '900',
  },
});
