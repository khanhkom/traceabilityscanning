import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
  Image,
  Keyboard,
  Alert,
} from 'react-native';
import ApiScanQR from '../../services/ApiScanQR';
import * as Animatable from 'react-native-animatable';
const {width, height} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import ListPost from '../ListPost';
const Home = ({navigation}) => {
  const [loading, setLoading] = useState(false);
  const [firstLoading, setFirstLoading] = useState(false);
  const [textinput, setTextinput] = useState('');
  const [message, setMessage] = useState('');
  useEffect(() => {
    setMessage('');
  }, []);

  const getAPI = async () => {
    let body = {
      message: textinput,
      // message: 'SRGTR_SRR6S_20200831_1',
    };
    let res = await ApiScanQR.APIScanQRcode(body);
    if (res && res.message) {
      navigation.navigate('DetailProductScreen', {
        message: res.message,
      });
    } else {
      Alert.alert(
        'Thông báo',
        'Mã quét không tồn tại',
        [
          {
            text: 'Thoát',
            onPress: () => setTextinput(''),
            style: 'Thoát',
          },
        ],
        {cancelable: false},
      );
    }
  };
  const searchMessage = () => {
    if (textinput != '') {
      getAPI();
    } else {
      Alert.alert(
        'Thông báo',
        'chưa có mã nào!!!',
        [
          {
            text: 'Thoát',
            // onPress: () => console.log('Cancel Pressed'),
            style: 'Thoát',
          },
        ],
        {cancelable: false},
      );
    }
  };
  // useEffect(() => {
  //   let codeURL =
  //     'https://bt.lina.supply/traceability?lotProducName=SRGTR_SRR6S_20200831_1&lang=vi';

  //   var mySubString = codeURL.substring(
  //     codeURL.lastIndexOf('lotProductName=') + 15,
  //     codeURL.lastIndexOf('&lang=vi'),
  //   );
  //   console.log('ket quả nè1', mySubString); //is the matched group if found
  // }, []);
  if (firstLoading) {
    return (
      <View
        style={{
          marginTop: height / 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator color="#009387" size="large" />
      </View>
    );
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      {loading && (
        <View style={styles.loadingbox}>
          <ActivityIndicator color="#fff" size="large" />
        </View>
      )}
      <View style={styles.header}>
        <Image
          source={require('../../../assets/home/logo1.png')}
          style={styles.logo}
        />
        <Text style={styles.logoText}>Lina Farmtrust</Text>
      </View>
      <View style={styles.line2} />
      <View animation="fadeInUpBig" style={[styles.footer]}>
        {/* <View style={{marginVertical: 16}}>
          <Text style={styles.textheader}>TRUY XUẤT NGUỒN GỐC</Text>
          {/* <Text>
            Giải pháp truy xuất nguồn gốc tích hợp tiêu chuẩn Quốc Tế dựa trên
            nền tảng công nghệ Blockchain tin tưởng hàng đầu thế giới
          </Text>
        </View> */}

        {/* <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}> */}
        <View style={styles.scanSection}>
          <TouchableOpacity
            onPress={() => navigation.navigate('ScanQRcodeScreen')}
            style={styles.buttonQRcode}>
            <AntDesign name="camerao" color={'#fff'} size={25} />
            <Text style={styles.text_QRcode}>Quét QRcode</Text>
          </TouchableOpacity>

          <View style={styles.line}>
            <View style={styles.horizontalLines} />
            <Text>Hoặc</Text>
            <View style={styles.horizontalLines} />
          </View>

          <View style={styles.textInputbox}>
            <View style={{marginLeft: 12}}>
              <FontAwesome name="search" color="#808080" size={22} />
            </View>

            <TextInput
              style={styles.boxinput}
              placeholder="Nhập lô sản phẩm..."
              placeholderTextColor="gray"
              onChangeText={(value) => setTextinput(value)}
              onEndEditing={() => {
                searchMessage();
                Keyboard.dismiss();
              }}
            />
            <TouchableOpacity
              style={{flex: 1.5 / 10}}
              onPress={() => {
                searchMessage();
                Keyboard.dismiss();
              }}>
              <Text style={{fontSize: 16, color: 'black'}}>Tìm</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.line2} />
        <ListPost navigation={navigation} />
        {/* </ScrollView> */}
      </View>
    </SafeAreaView>
  );
};

export default Home;
