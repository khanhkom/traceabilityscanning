import {Dimensions, StyleSheet, Platform} from 'react-native';
import {getFont, getWidth, HEIGHT, WIDTH} from '../../configs/functions';
const {width, height} = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  logoText: {
    marginLeft: WIDTH(50),
    color: '#000',
    fontSize: getFont(22),
    fontWeight: 'bold',
  },
  logo: {
    height: HEIGHT(60),
    width: HEIGHT(60),
  },
  header: {
    // flex: 1,
    // justifyContent: 'flex-end',
    paddingHorizontal: 20,
    backgroundColor: '#fff',
    height: HEIGHT(100),
    flexDirection: 'row',
    alignItems: 'center',
    // paddingBottom: 50,
  },
  textheader: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 12,
  },
  footer: {
    flex: 0.75,
    // borderTopLeftRadius: 30,
    // borderTopRightRadius: 30,
  },
  scanSection: {
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingVertical: 15,
    marginTop: HEIGHT(10),
  },
  text_QRcode: {
    color: '#fff',
    fontSize: 18,
    marginLeft: 16,
  },
  horizontalLines: {
    height: 1,
    width: width / 3,
    backgroundColor: 'black',
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  buttonQRcode: {
    width: WIDTH(300),
    height: HEIGHT(80),
    backgroundColor: '#1db853',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  line: {
    height: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  textInputbox: {
    width: WIDTH(300),
    height: 50,
    backgroundColor: '#F0F0F0',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    marginTop: 16,
    alignSelf: 'center',
  },
  boxinput: {
    color: 'black',
    flex: 7 / 10,
    justifyContent: 'center',
    color: 'black',
    fontSize: 16,
  },
  loadingbox: {
    position: 'absolute',
    marginTop: height / 2,
    marginLeft: width / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  line2: {
    height: HEIGHT(8),
    width: getWidth(),
    backgroundColor: '#C1C1C1',
  },
});
