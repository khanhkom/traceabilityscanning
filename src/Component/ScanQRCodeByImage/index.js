import React, {useState} from 'react';
// Import required components
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';

// Import Image Picker
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import RNQRGenerator from 'rn-qr-generator';
import HeaderBack from '../../common/header/HeaderBack';
import {getFont, HEIGHT, WIDTH} from '../../configs/functions';
import ApiScanQR from '../../services/ApiScanQR';
import Entypo from 'react-native-vector-icons/Entypo';
const ScanQRCodeByImage = ({navigation}) => {
  const [filePath, setFilePath] = useState({});
  const [qrCode, setQrCode] = useState('');

  const chooseFile = () => {
    let options = {
      title: 'Select Image',
      includeBase64: true,
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose Photo from Custom Option',
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, (response) => {
      // console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = response;
        // console.log('source_source', source);
        let base64 = 'data:image/jpeg;base64,' + response.base64;
        // console.log('base64_base64', base64);
        // You can also display the image using data:
        // let source = {
        //   uri: 'data:image/jpeg;base64,' + response.data
        // };
        setFilePath(source);
        onDetectQRCode(source.uri, base64);
      }
    });
  };
  const onDetectQRCode = (PATH_TO_IMAGE, imageBase64String) => {
    // console.log('PATH_TO_IMAGE_PATH_TO_IMAGE', imageBase64String);
    RNQRGenerator.detect({
      // uri: PATH_TO_IMAGE, // local path of the image. Can be skipped if base64 is passed.
      base64: imageBase64String, // If uri is passed this option will be skipped.
    })
      .then(async (response) => {
        const {values} = response; // Array of detected QR code values. Empty if nothing found.
        console.log('values_values', values);
        var mySubString = values[0].substring(
          values[0].lastIndexOf('lotProductName=') + 15,
          values[0].lastIndexOf('&lang=vi'),
        );
        let body = {
          message: mySubString,
        };
        console.log('body_body', body);
        let res = await ApiScanQR.APIScanQRcode(body);
        console.log('res_res', res);

        navigation?.navigate('DetailProductScreen', {
          message: res.message,
        });
        setQrCode(values);
      })
      .catch((error) => {
        console.log('error_error', error);
        Alert.alert(
          'Thông báo',
          'Mã quét không tồn tại',
          [
            {
              text: 'Thoát',
              onPress: () => navigation.navigate('Home'),
              style: 'Thoát',
            },
            {
              text: 'Quét lại',
              onPress: () => chooseFile(),
              style: 'Quét lại',
            },
          ],
          {cancelable: false},
        );
        // console.log('Cannot detect QR code in image', error)
      });
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <HeaderBack />
      <View style={styles.container}>
        <Text style={styles.titleText}>Chọn ảnh để truy xuất nguồn gốc</Text>
        <Text style={styles.textNormal}>
          Giải pháp truy xuất nguồn gốc tích hợp tiêu chuẩn Quốc Tế dựa trên nền
          tảng công nghệ Blockchain tin tưởng hàng đầu thế giới
        </Text>
        {/* <Image
          source={{
            uri: 'data:image/jpeg;base64,' + filePath.data,
          }}
          style={styles.imageStyle}
        /> */}
        {filePath.uri ? (
          <Image source={{uri: filePath.uri}} style={styles.imageStyle} />
        ) : (
          <View style={styles.boxNoImage}>
            <Entypo name="camera" color="#fff" size={WIDTH(50)} />
            <Text style={styles.textNoImage}>Chưa có ảnh</Text>
          </View>
        )}
        <Text style={styles.textStyle}>{qrCode}</Text>
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.buttonStyle}
          onPress={chooseFile}>
          <Text style={styles.textStyle}>Chọn ảnh</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default ScanQRCodeByImage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  textNoImage: {
    color: '#000',
    marginTop: HEIGHT(15),
  },
  textNormal: {
    textAlign: 'center',
  },
  titleText: {
    fontSize: getFont(22),
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 20,
  },
  textStyle: {
    padding: 10,
    color: '#fff',
    fontSize: getFont(20),
  },
  buttonStyle: {
    height: 50,
    width: WIDTH(300),
    backgroundColor: '#1db853',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 16,
    borderRadius: 10,
  },
  imageStyle: {
    width: 200,
    height: 200,
    margin: 5,
    borderRadius: 10,
    marginTop: HEIGHT(30),
  },
  boxNoImage: {
    width: 200,
    height: 200,
    margin: 5,
    backgroundColor: '#ddd',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: HEIGHT(30),
  },
});
