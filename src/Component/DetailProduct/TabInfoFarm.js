import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
  Image,
  Keyboard,
  Alert,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
const {width, height} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import moment from 'moment';
const TabInfoFarm = ({dataInfo, certificate, soil}) => {
  return (
    <>
      <View style={{margin: 12}}>
        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Tên:</Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {dataInfo.farmInfo.name}
          </Text>
        </View>

        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Địa chỉ:</Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {dataInfo.farmInfo.address}
          </Text>
        </View>

        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Email:</Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {dataInfo.farmInfo.email}
          </Text>
        </View>

        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Số điện thoại:</Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {dataInfo.farmInfo.phone}
          </Text>
        </View>
        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            Địa điểm trồng:
          </Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {dataInfo.farmInfo.sectionLand}
          </Text>
        </View>
        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Chứng chỉ:</Text>
          {certificate.map((item) => {
            return (
              <View>
                <Text style={{fontSize: 16, marginLeft: 12}}>{item.name}</Text>
              </View>
            );
          })}
        </View>
        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Kiểm nghiệm:</Text>
        </View>
        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}> • Đất:</Text>
        </View>
        <View style={[styles.texttitle, {marginRight: 100}]}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            - Ngày phát hành kết quả:
          </Text>

          <Text style={{fontSize: 16, marginLeft: 12}}>
            {moment(soil.publishedDate).format('YYYY-MM-DD')}
          </Text>
        </View>
        <View style={[styles.texttitle, {marginRight: 100}]}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            - Khiểm nghiệm bởi:
          </Text>
          <Text style={{fontSize: 16, marginLeft: 12, marginRight: 16}}>
            {soil.issuedBy}
          </Text>
        </View>
        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>• Nước:</Text>
        </View>
        <View style={[styles.texttitle, {marginRight: 100}]}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            - Ngày phát hành kết quả:
          </Text>

          <Text style={{fontSize: 16, marginLeft: 12}}>
            {moment(dataInfo.farmInfo.water.publishedDate).format('YYYY-MM-DD')}
          </Text>
        </View>
        <View style={[styles.texttitle, {marginRight: 100}]}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            - Khiểm nghiệm bởi:
          </Text>
          <Text style={{fontSize: 16, marginLeft: 12, marginRight: 16}}>
            {dataInfo.farmInfo.water.issuedBy}
          </Text>
        </View>
        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>• Thành phẩm:</Text>
        </View>
        <View style={[styles.texttitle, {marginRight: 100}]}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            - Ngày phát hành kết quả:
          </Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {moment(dataInfo.farmInfo.finishedProduct.publishedDate).format(
              'YYYY-MM-DD',
            )}
            {/* {dataInfo.farmInfo.finishedProduct.publishedDate} */}
          </Text>
        </View>
        <View style={[styles.texttitle, {marginRight: 100}]}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            - Khiểm nghiệm bởi:
          </Text>
          <Text style={{fontSize: 16, marginLeft: 12, marginRight: 16}}>
            {dataInfo.farmInfo.finishedProduct.issuedBy}
          </Text>
        </View>
      </View>
    </>
  );
};

export default TabInfoFarm;
