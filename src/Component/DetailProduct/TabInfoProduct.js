import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
  Image,
  Keyboard,
  Alert,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
const {width, height} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import moment from 'moment';
const TabInfoProduct = ({finishedProductInfo}) => {
  return (
    <>
      <View style={{margin: 12}}>
        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Mã sản phẩm:</Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {finishedProductInfo.finishedProductCode}
          </Text>
        </View>

        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            Tên thành phầm:
          </Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {finishedProductInfo.finishedProductName}
          </Text>
        </View>

        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Mã phân loại:</Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {finishedProductInfo.gradedCode}
          </Text>
        </View>

        <View style={styles.texttitle}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Tên phân loại:</Text>
          <Text style={{fontSize: 16, marginLeft: 12}}>
            {finishedProductInfo.gradedName}
          </Text>
        </View>
      </View>
    </>
  );
};

export default TabInfoProduct;
