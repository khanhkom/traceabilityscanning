import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
  Image,
  Keyboard,
  Alert,
} from 'react-native';
import ApiScanQR from '../../services/ApiScanQR';
import * as Animatable from 'react-native-animatable';
const {width, height} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import {roundToNearestPixel} from 'react-native/Libraries/Utilities/PixelRatio';
import {LongPressGestureHandler} from 'react-native-gesture-handler';
import TabInfoFarm from './TabInfoFarm';
import TabInfoProduct from './TabInfoProduct';
import moment from 'moment';
// import Moment from 'react-moment';
const DetailProduct = ({navigation, route}) => {
  const [loadingInfo, setLoadingInfo] = useState(true);
  const [loadingTimeline, setLoadingTimeline] = useState(true);
  const [isshowfame, setIsshowfame] = useState(false);
  const [isshowproduct, setIsshowproduct] = useState(false);
  const [dataInfo, setDataInfo] = useState([]);
  const [dataTimeline, setDataTimeline] = useState([]);
  const [certificate, setCertificate] = useState([]);
  const [soil, setSoil] = useState([]);
  const [finishedProductInfo, setFinishedProductInfo] = useState([]);
  const [lotProducts, setLotProducts] = useState([]);
  useEffect(() => {
    setIsshowfame(false);
    setIsshowproduct(false);
    getAPITracabilityInfo();
    getAPITracabilityTimeline();
  }, []);

  const getAPITracabilityInfo = async () => {
    let {message} = route.params;
    console.log(message);
    let body = {
      lotProductId: message,
      languageId: '1',
    };
    let res = await ApiScanQR.APIgetTracabilityInfo(body);
    if (res) {
      console.log(res);
      setDataInfo(res);
      setCertificate(res.farmInfo.certificates);
      setSoil(res.farmInfo.soil);
      setFinishedProductInfo(res.finishedProductInfo);
      setLotProducts(res.lotProducts);
      setLoadingInfo(false);
    } else {
      // setLoadingInfo(false);
      Alert.alert(
        'Thông báo',
        'Mã quét không tồn tại',
        [
          {
            text: 'Thoát',
            onPress: () => navigation.navigate('Home'),
            style: 'Thoát',
          },
        ],
        {cancelable: false},
      );
    }
  };
  const getAPITracabilityTimeline = async () => {
    let {message} = route.params;
    let body = {
      lotProductId: message,
      languageId: '1',
    };
    let res = await ApiScanQR.APIgetTracabilityTimeline(body);
    if (res) {
      console.log(res);
      setLoadingTimeline(false);
      setDataTimeline(res.timeLine);
    } else {
      // setLoadingTimeline(false);
      Alert.alert(
        'Thông báo',
        'Mã quét không tồn tại',
        [
          {
            text: 'Thoát',
            onPress: () => navigation.navigate('Home'),
            style: 'Thoát',
          },
        ],
        {cancelable: false},
      );
    }
  };
  if (loadingInfo || loadingTimeline)
    return (
      <View
        style={{
          marginTop: height / 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator color="#009387" size="large" />
      </View>
    );
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />

      <View style={styles.header}>
        <Image
          source={require('../../../assets/home/logo.png')}
          style={styles.Imageheader}
        />
      </View>

      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <Text style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}>
          TRUY XUẤT NGUỒN GỐC
        </Text>
        <View
          style={{
            width: width - 40,
            height: 2,
            backgroundColor: 'black',
            marginVertical: 12,
          }}
        />
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <TouchableOpacity
            onPress={() => setIsshowproduct(!isshowproduct)}
            style={styles.bottomshow}>
            <View style={{flex: 8 / 10}}>
              <Text style={{fontSize: 18, color: 'white'}}>
                Thông tin thành phẩm
              </Text>
            </View>

            <View style={{flex: 1 / 10}}>
              {isshowproduct == true ? (
                <FontAwesome name="angle-up" color="white" size={30} />
              ) : (
                <FontAwesome name="angle-down" color="white" size={30} />
              )}
            </View>
          </TouchableOpacity>
          {isshowproduct === true ? (
            <>
              <TabInfoProduct finishedProductInfo={finishedProductInfo} />
              <View
                style={{
                  width: width - 40,
                  height: 0.5,
                  backgroundColor: 'black',
                }}
              />
            </>
          ) : null}

          <TouchableOpacity
            onPress={() => setIsshowfame(!isshowfame)}
            style={styles.bottomshow}>
            <View style={{flex: 8 / 10}}>
              <Text style={{fontSize: 18, color: 'white'}}>
                Thông tin nông trại
              </Text>
            </View>

            <View style={{flex: 1 / 10}}>
              {isshowfame == true ? (
                <FontAwesome name="angle-up" color="white" size={30} />
              ) : (
                <FontAwesome name="angle-down" color="white" size={30} />
              )}
            </View>
          </TouchableOpacity>
          {isshowfame === true ? (
            <>
              <TabInfoFarm
                dataInfo={dataInfo}
                soil={soil}
                certificate={certificate}
              />
              <View
                style={{
                  width: width - 40,
                  height: 0.5,
                  backgroundColor: 'black',
                }}
              />
            </>
          ) : null}

          {lotProducts.map((item) => {
            return (
              <View style={{margin: 12}}>
                <View style={styles.texttitle}>
                  <Text style={{fontWeight: 'bold', fontSize: 16}}>
                    Lô sản phẩm:
                  </Text>
                  <View>
                    <Text style={{fontSize: 16, marginLeft: 12}}>
                      {item.name}
                    </Text>
                  </View>
                </View>

                <View style={styles.texttitle}>
                  <Text style={{fontWeight: 'bold', fontSize: 16}}>
                    Lô đất trồng:
                  </Text>
                  <Text style={{fontSize: 16, marginLeft: 12}}>
                    {item.sectionLand}
                  </Text>
                </View>
                <View style={styles.texttitle}>
                  <Text style={{fontWeight: 'bold', fontSize: 16}}>
                    Mùa vụ:
                  </Text>
                  <Text style={{fontSize: 16, marginLeft: 12}}>
                    {item.season}
                  </Text>
                </View>

                <View style={styles.texttitle}>
                  <Text style={{fontWeight: 'bold', fontSize: 16}}>Vụ:</Text>
                  <Text style={{fontSize: 16, marginLeft: 12}}>
                    {item.season}
                  </Text>
                </View>
              </View>
            );
          })}
          <View
            style={{width: width - 40, height: 0.5, backgroundColor: 'black'}}
          />
          <View style={{marginVertical: 16}}>
            <View style={styles.texttitle}>
              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                MỐC THỜI GIAN:
              </Text>
              <Text style={{fontSize: 16, marginLeft: 12}}></Text>
            </View>
          </View>
          {dataTimeline.map((item) => {
            return (
              <View
                style={[
                  styles.texttitle,
                  {marginRight: 100, marginVertical: 16},
                ]}>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>
                  - Thời gian:
                </Text>
                <View>
                  <Text style={{fontSize: 16, marginLeft: 12}}>
                    {moment(item.time).format('YYYY-MM-DD')}
                    {/* {dataInfo.farmInfo.finishedProduct.publishedDate} */}
                  </Text>
                  {item.type ? (
                    <Text style={{fontSize: 16, marginLeft: 12}}>
                      {item.type}
                      {/* {dataInfo.farmInfo.finishedProduct.publishedDate} */}
                    </Text>
                  ) : null}
                </View>
              </View>
            );
          })}
          {/* <View
            style={{width: width - 40, height: 0.5, backgroundColor: 'black'}}
          /> */}
        </ScrollView>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home')}
          style={{
            borderRadius: 20,
            height: 50,
            width: width - 40,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#1db853',
          }}>
          <Text style={{fontWeight: 'bold', fontSize: 16, color: 'white'}}>
            Màn hình chính
          </Text>
        </TouchableOpacity>
      </Animatable.View>
    </SafeAreaView>
  );
};

export default DetailProduct;
