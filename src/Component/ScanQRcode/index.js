import React, {Component} from 'react';

import {View, Dimensions, Text, Alert, ActivityIndicator} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Icon from 'react-native-vector-icons/AntDesign';
import * as Animatable from 'react-native-animatable';
import ApiScanQR from '../../services/ApiScanQR';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {RNCamera} from 'react-native-camera';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

class ScanQRcode extends Component {
  state = {
    flashMode: false,
    firstLoading: true,
  };
  async onSuccess(e) {
    console.log('scan QRcode', e);
    let codeURL = '';
    if (e) {
      codeURL = e.data;
      var mySubString = codeURL.substring(
        codeURL.lastIndexOf('lotProductName=') + 15,
        codeURL.lastIndexOf('&lang=vi'),
      );
      let body = {
        message: mySubString,
      };
      let res = await ApiScanQR.APIScanQRcode(body);
      if (res && res.message) {
        this.props.navigation?.navigate('DetailProductScreen', {
          message: res.message,
        });
      } else {
        Alert.alert(
          'Thông báo',
          'Mã quét không tồn tại',
          [
            {
              text: 'Thoát',
              onPress: () => this.props.navigation.navigate('Home'),
              style: 'Thoát',
            },
            {
              text: 'Quét lại',
              onPress: () => this.clickRescanner(),
              style: 'Quét lại',
            },
          ],
          {cancelable: false},
        );
      }
      console.log('code QR', mySubString); //is the matched group if found
    }
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: 0,
      },
      to: {
        [translationType]: fromValue,
      },
    };
  }
  clickRescanner = () => {
    console.log('reScanner');
    this.scanner.reactivate();
  };
  componentDidMount() {
    setTimeout(() => {
      this.setState({firstLoading: false});
    }, 200);
  }
  render() {
    const {firstLoading} = this.state;
    if (firstLoading) {
      return <ActivityIndicator size="large" color="#000" />;
    }
    return (
      <QRCodeScanner
        // reactivate={true}
        ref={(node) => {
          this.scanner = node;
        }}
        showMarker
        onRead={this.onSuccess.bind(this)}
        cameraStyle={{height: SCREEN_HEIGHT}}
        flashMode={
          this.state.flashMode == true
            ? RNCamera.Constants.FlashMode.torch
            : null
        }
        customMarker={
          <View style={styles.rectangleContainer}>
            <View style={styles.topOverlay}>
              <TouchableOpacity
                onPress={() =>
                  // this.props.navigation.navigate('Home')
                  this.props.navigation.navigate('ScanQRCodeByImage')
                }
                style={{
                  height: 50,
                  width: SCREEN_WIDTH - 32,
                  backgroundColor: '#1db853',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 16,
                  borderRadius: 10,
                }}>
                <Text style={{color: 'white', fontSize: 18}}>
                  Chọn ảnh từ thư viện
                </Text>
              </TouchableOpacity>
              <Text style={{fontSize: 30, color: 'white'}}>
                QR CODE SCANNER
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={styles.leftAndRightOverlay} />

              <View style={styles.rectangle}>
                <Animatable.View
                  style={styles.scanBar}
                  direction="alternate-reverse"
                  iterationCount="infinite"
                  duration={1700}
                  easing="linear"
                  animation={this.makeSlideOutTranslation(
                    'translateY',
                    SCREEN_WIDTH * 0.65,
                  )}
                />
                <View
                  style={{
                    height: rectDimensions / 3,
                    width: rectDimensions,
                    flexDirection: 'row',
                  }}>
                  <View
                    style={{
                      width: rectDimensions / 3,
                      borderLeftColor: '#1db853',
                      borderLeftWidth: 3,
                      borderTopColor: '#1db853',
                      borderTopWidth: 3,
                    }}
                  />
                  <View
                    style={{
                      width: rectDimensions / 3,
                    }}
                  />
                  <View
                    style={{
                      width: rectDimensions / 3,
                      borderRightColor: '#1db853',
                      borderRightWidth: 3,
                      borderTopColor: '#1db853',
                      borderTopWidth: 3,
                    }}
                  />
                </View>
                <View
                  style={{
                    height: rectDimensions / 3,
                    width: rectDimensions,
                    flexDirection: 'row',
                  }}
                />

                <View
                  style={{
                    height: rectDimensions / 3,
                    width: rectDimensions,
                    flexDirection: 'row',
                  }}>
                  <View
                    style={{
                      width: rectDimensions / 3,
                      borderLeftColor: '#1db853',
                      borderLeftWidth: 3,
                      borderBottomColor: '#1db853',
                      borderBottomWidth: 3,
                    }}
                  />
                  <View
                    style={{
                      width: rectDimensions / 3,
                    }}
                  />
                  <View
                    style={{
                      width: rectDimensions / 3,
                      borderRightColor: '#1db853',
                      borderRightWidth: 3,
                      borderBottomColor: '#1db853',
                      borderBottomWidth: 3,
                    }}
                  />
                </View>
              </View>
              <View style={styles.leftAndRightOverlay} />
            </View>
            <View style={styles.bottomOverlay}>
              <TouchableOpacity
                style={{marginLeft: 50, marginVertical: 12}}
                onPress={() => {
                  this.setState({
                    flashMode: !this.state.flashMode,
                  });
                }}>
                {this.state.flashMode == true ? (
                  <MaterialCommunityIcons
                    name="flashlight-off"
                    color="#808080"
                    size={40}
                  />
                ) : (
                  <MaterialCommunityIcons
                    name="flashlight"
                    color="#1db853"
                    size={40}
                  />
                )}
              </TouchableOpacity>
            </View>
          </View>
        }
      />
    );
  }
}

const overlayColor = 'rgba(0,0,0,0.5)'; // this gives us a black color with a 50% transparency

const rectDimensions = SCREEN_WIDTH * 0.65; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.005; // this is equivalent to 2 from a 393 device width
const rectBorderColor = 'red';

const scanBarWidth = SCREEN_WIDTH * 0.65; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.006; //this is equivalent to 1 from a 393 device width
const scanBarColor = '#22ff00';

const iconScanColor = 'blue';

const styles = {
  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    // borderWidth: rectBorderWidth + 1,
    // borderColor: rectBorderColor,
    // borderRadius: 20,
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: 'center',
    alignItems: 'center',
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25,
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
  },

  scanBar: {
    position: 'absolute',
    top: 0,
    width: scanBarWidth - 3,
    height: scanBarHeight,
    backgroundColor: scanBarColor,
    // bottom: 0,
  },
};
export default ScanQRcode;
