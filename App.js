import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import NavigationStack from './src/router/index';
import Home from './src/Component/Home';
const App = () => {
  return <NavigationStack />;
};

export default App;
